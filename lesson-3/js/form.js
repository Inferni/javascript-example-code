/**---------------------------
 * DOM Elements
 ------------------------------*/
 const loginFormElement = document.getElementById("login-form");

 /**---------------------------
 * Event handlers
 ------------------------------*/
 /**
  * Handle the form submission
  * @param {Event} event 
  */
 function onFormSubmit(event) { // Event
     // Stop the default behaviour!
     event.preventDefault();
     console.log("I hacked the form! 🚨");
     const formData = new FormData(loginFormElement);
     const username = formData.get("username");
     console.log(username);
 }
 loginFormElement.addEventListener("submit", onFormSubmit);
