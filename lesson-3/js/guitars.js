async function fetchGuitars() {
  try {
    const response = await fetch(
      "https://dce-noroff-api.herokuapp.com/guitars"
    );
    const json = await response.json(); // JavaScript Object Notation
    return json; //  Guitars
  } catch (error) {
    console.error("Error: ", error.message);
  }
}

// DOM Elements
const guitarsSelectElement = document.getElementById("guitars");
const guitarTitleElement = document.getElementById("guitar-title");
const guitarImageElement = document.getElementById("guitar-image");
const guitarMaterialsElement = document.getElementById("guitar-materials");

// Can use await inside a script marked as type=module
const guitars = await fetchGuitars();

for (const guitar of guitars) {
  // Add to the select.
  const html = `<option value=${guitar.id}>${guitar.model}</option>`;
  guitarsSelectElement.insertAdjacentHTML("beforeend", html);
}

/** @type HTMLSelectElement */
function onSelectChange() {
  const guitarId = this.value;
  const guitar = guitars.find((guitar) => guitar.id === guitarId);
  renderSelectedGuitar(guitar);
}
guitarsSelectElement.addEventListener("change", onSelectChange);

const renderSelectedGuitar = (guitar) => {
  guitarTitleElement.innerText = guitar.model + " by " + guitar.manufacturer;
  guitarImageElement.src = guitar.image;
  guitarMaterialsElement.innerHTML = "";
  for (const key in guitar.materials) {
    const material = guitar.materials[key];
    guitarMaterialsElement.insertAdjacentHTML(
      "beforeend",
      `<li>${material}</li>`
    );
  }
};
