class Animal {
    // Properties
    #type;
    name = "";
    emoji = "";

    constructor(type, name, emoji) {
        this.#type = type;
        this.name = name;
        this.emoji = emoji;
    }

    // Methods
    getType() {
        return this.#type;
    }

    printInfo() {
        console.log(this.name + " is a " + this.#type + " and looks like " + this.emoji);
    }
}

const cat1 = new Animal("cat", "Felix", "😾");
cat1.printInfo();

const cat2 = new Animal("cat", "Garfield", "🐈");
cat2.printInfo();

const dog = new Animal("dog", "Oliver", "🐶");
dog.printInfo();












function AnimalFC(type, name, emoji) {
    this.type = type;
    this.name = name;
    this.emoji = emoji;
}

AnimalFC.prototype.printInfo = function() {
    console.log(this.name + " is a " + this.type + " and looks like " + this.emoji);
}

const croc = new AnimalFC("croc", "Leroy", "🐊");
croc.printInfo();

console.log(croc);