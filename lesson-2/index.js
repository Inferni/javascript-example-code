// Functional Constructor - DRY
function Todo(id, title) {
    this.id = id;
    this.title = title;
    this.completed = false;
    // FC must NOT have a return.
}

/**----------------------------------------------------
 * Variables
 ----------------------------------------------------*/

// Our list of todos!!
const todos = [];
// Using the new keyword
// const todo = new Todo(1, "First todo");
// console.log(todo);
// Output { id: 1, title: "First todo", completed: false }

/**----------------------------------------------------
 * DOM ELEMENTS
 ----------------------------------------------------*/
const addButtonElement = document.getElementById("add-todo");
const newTodoInputElement = document.getElementById("new-todo");
const todoListElement = document.getElementById("todo-list");

/**----------------------------------------------------
 * Functions
 ----------------------------------------------------*/
function createId() {
    return Math.random().toString(16).slice(2);
}

function renderTodos(todos) {
    todoListElement.innerHTML = "";
    /**for (let i = 0; i < todos.length; i++) {
        const todo = todos[i];
        const liElement = document.createElement("li");
        liElement.innerText = todo.title + ": " + todo.completed;
        todoListElement.appendChild(liElement);
    } */
    /**for (const todo of todos) {
        const liElement = document.createElement("li");
        liElement.innerText = todo.title + ": " + todo.completed;
        todoListElement.appendChild(liElement);
    }*/

    for (const todo of todos) {
         // Template Literals - Interpolation
        todoListElement.innerHTML += `
            <li>${todo.title} - ${todo.completed}</li>
        `;
    }
}

/**----------------------------------------------------
 * Event Handlers
 ----------------------------------------------------*/
function onButtonClick() {
    // Get the value from the input
    const newTodoTitle = newTodoInputElement.value.trim();
    if (newTodoTitle === "") {
        alert("How dare you! Hackerman!!");
        newTodoInput.value = "";
        return;
    }
    
    // Create a new todo
    const newId = createId();
    const newTodo = new Todo(newId, newTodoTitle);
    // Add it to a list of todos
    todos.push(newTodo);
    // Update visually, the HTML
    renderTodos(todos);
}
addButtonElement.addEventListener("click", onButtonClick);






/**----------------------------------------------------
 * Some other stuff
 ----------------------------------------------------*/
 // Casing
// kebab-case
// camelCase <-- 
// PascalCase <-- Classes, Functional Constructors
// SNAKE_CASE_UPPER_CASE <- Constant 

const myOtherTodo = new Todo(2, "Hello");

/// Mutability - Immutability

// Reassignment

const message = { text: "Hello" };
// message = { text: "Bye" }; // Reassign - Wont work
message.text = "Bye" // Mutate - Will Work
