const TIME_TRAVEL_FORWARD = "forward";
const TIME_TRAVEL_BACKWARDS = "backwards";

// Global Scope!
// Data storage - Dynamically Typed.
let myAge = 39; // Can be re-assigned
 // myAge = myAge + 1; // I'm older now. 
 // myAge++;
 // myAge += 1; // Syntactical Sugar 🧁

// Go Forward in Time
function goForwardInTime() {
    myAge += 1;
}

// Go Backwards in Time
function goBackwardsInTime() {
    myAge -= 1;
}

function timeTravel(direction) {
    if (direction === TIME_TRAVEL_FORWARD) { // MAgic String
        goForwardInTime();
    } 
    else if (direction === TIME_TRAVEL_BACKWARDS) {
        goBackwardsInTime();
    }
    else {
        console.error(direction + " is not a valid time travel direction!");
    }
    currentAgeElement.innerText = myAge;
}






function timeTravelWithSwitch(direction) {
   
    switch(direction) {

        case TIME_TRAVEL_FORWARD:
            goForwardInTime();
            break; // Stop the checks! i've got it

        case TIME_TRAVEL_BACKWARDS: 
            goBackwardsInTime();
            break;
        
        default:
            throw new Error("That is an invalid direction.");
    }


    currentAgeElement.innerText = myAge;
}














const currentAgeElement = document.getElementById("current-age");
currentAgeElement.innerText = myAge;

const travelForwardButton = document.getElementById("travel-forward");
const travelBackwardsButton = document.getElementById("travel-backwards");

function onTravelForwardClick() {
    console.log(this); // To the element that triggered the event.
    timeTravel(TIME_TRAVEL_FORWARD);
}
travelForwardButton.addEventListener("click", onTravelForwardClick);

function onTravelBackwardsClick() {
   timeTravel(TIME_TRAVEL_BACKWARDS);
}
travelBackwardsButton.addEventListener("click", onTravelBackwardsClick);

const myHeight = 170;
// myHeight = 180; // Illegal 🚓

// HTML references
// DOM - Document Object Model - API
const titleElement = document.getElementById("title");
titleElement.innerText = "The Best JavaScript App Ever";

const myBioElement = document.getElementById("my-bio");
myBioElement.innerText =
  "I am " +
  myAge +
  " years old. My height is " +
  myHeight +
  "cm. The tallest man in this chair.";

  // Objects
const person = {
    // Properties
    age: 39,
    height: 170,
    name: "Dewald Els", // trailing comma
    // Methods
    printInfo() {
        console.log(this); // THe Object!
        console.log("person name is: ", this.name);
    }
}  

person.printInfo();

